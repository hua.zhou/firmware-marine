//
// Created by eric on 17-9-28.
//

#ifndef __APPS_PX4_MIXER_H
#define __APPS_PX4_MIXER_H

extern "C" __EXPORT int do_mixer_switch(const char *devname, const char *fname);

#endif //__APPS_PX4_MIXER_H
